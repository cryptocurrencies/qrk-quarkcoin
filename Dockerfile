# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/quark-project/quark.git /opt/quarkcoin && \
    cd /opt/quarkcoin && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --without-gui && \
    make

# ---- Release ---- #
FROM ubuntu:14.04 as release 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r quarkcoin && useradd -r -m -g quarkcoin quarkcoin
RUN mkdir /data
RUN chown quarkcoin:quarkcoin /data
COPY --from=build /opt/quarkcoin/src/quarkd /usr/local/bin/
COPY --from=build /opt/quarkcoin/src/quark-cli /usr/local/bin/
USER quarkcoin
VOLUME /data
EXPOSE 8373 8372
CMD ["/usr/local/bin/quarkd", "-datadir=/data", "-conf=/data/quarkcoin.conf", "-server", "-txindex", "-printtoconsole"]