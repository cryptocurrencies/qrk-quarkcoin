This Dockerfile generates the latest Quarkcoin client used in server applications. It is anticiapted that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name quarkcoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/qrk-quarkcoin:latest
```